from flask import Flask
app = Flask(__name__)

# This file does not have to be used in the application
# Choice of the web server is up to the candidate

# Required endpoints:


@app.route('/predict')
def predict():
    # the endpoint has to take n previous values of the traffic to predict the new one
    return 'Prediction result from the model'


@app.route('/metrics')
def metrics():
    return 'Request per seconds to the /predict endpoint'
